# Generalized Measures of Population Synchrony

This repository contains the data and code needed to reproduce the results and figures reported in [1]. In this work the authors
define and study a notion of population synchrony.

Let $(M,d)$ be a compact metric space, and let $\pi \in \mathcal{P}(M)$ be a probability measure representing the distribution of a population over $M$. 
Then the synchrony of $\pi$ is defined to be
$$
F(\pi) = 1 - \frac{1}{\nu_{(M,d)}} \inf_{\alpha \in M} \left(\int_{M} d(x,\alpha)^2  d\pi(x)\right)^{1/2},
$$
where the synchrony normalization constant
$$
\nu_{(M,d)} = \sup_{\mu \in \mathcal{P}(M)} \inf_{\alpha \in M} \left(\int_{M} d(x,\alpha)^2  d\mu(x)\right)^{1/2}.
$$

## Dependencies

The code contained in this reporitory depends on several Python packages including `popsynch` (installable using `$ pip popsynch[notebooks]==0.2`). To ensure reproducibility create a [conda](https://anaconda.org/anaconda/conda) 
environment with the provided `environment.yml` file.
```
$ conda env create -f environment.yml
$ conda activate popsynch
```

## Contents

Analyses are performed in Jupyter Notebooks contained in `src/` and Figures are output to `figs/`.

## Citations

[1] Motta, F.C., McGoff, K., Cummins, B., & Haase, S.B.,  (2024). Generalized Measures of Population Synchrony. _Mathematical Biosciences_, 109344. [doi:10.1016/j.mbs.2024.109344](https://doi.org/10.1016/j.mbs.2024.109344)